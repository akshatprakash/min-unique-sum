import java.util.*;

public class MinUniqueSum {

    public static void main(String[] args) {
        int arr[] = {3, 1, 2, 2};
        System.out.println(getMinimumUniqueSum(arr));
    }
    static int getMinimumUniqueSum(int[] arr) {
        int min = 2000;
        int max = 1;
        int sum = 0;
        Map<Integer, Integer> duplicateValuesMap = new HashMap<>();
        Set<Integer> sequence = new TreeSet<>();
        Set<Integer> missingValues = new TreeSet<>();
        Set<Integer> arrSet = new HashSet<>();

        for (int i=1; i<=min; i++) {
            sequence.add(i);
        }

        for (int i=1; i<=arr[0]; i++) {
            for (int j=arr[0]; j>i; j--) {
                if (arr[i] == arr[j]) {
                    duplicateValuesMap.put(j, arr[j]);
                }
                if (arr[i]<min) {
                    min = arr[i];
                }
                if (arr[i]>max) {
                    max = arr[i];
                }
                sequence.remove(arr[i]);
            }
            arrSet.add(arr[i]);
        }

        Iterator<Integer> itr = sequence.iterator();
        while (itr.hasNext()) {
            Integer val = itr.next();
            if (val >= min && val <= max) {
                missingValues.add(val);
            }
        }
        for (Map.Entry<Integer, Integer> entry: duplicateValuesMap.entrySet()) {
            Integer replacementValue;
            if (itr.hasNext()) {
                replacementValue = itr.next();
            } else {
                replacementValue = max+1;
            }
            arrSet.add(replacementValue);
        }
        for (Integer val : arrSet) {
            sum+=val;
        }
        return sum;
    }
}
